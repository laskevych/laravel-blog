<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('names', 'NamesController@index');
Route::get('names/{id}', 'NamesController@show');

Route::get('books', 'BooksController@index');
Route::post('books', 'BooksController@sendForm');

Route::get('users', 'UserController@index')->name('users');
Route::get('users/{userId}', 'UserController@getUser')->name('user');
Route::get('users/{userId}/comments', 'UserController@getUserComments')->name('user_comments');
Route::get('users/has-comments', 'UserController@getUsersHasComments');
Route::get('users/add', function (){
    return view('form');
});
Route::post('users/add', 'UserController@addUser');

Route::get('comments', 'CommentController@index')->name('comments');
Route::get('comment/{commentId}/user', 'CommentController@getUser')->name('get_user');
Route::get('comment/{commentId}/book', 'CommentController@getBook')->name('get_book');

Route::get('users/{userId}/roles', 'UserController@getRolesByUser');
Route::get('role/{roleId}/users', 'RoleController@getUsersByRole');

/*Ко многим через*/
#Route::get('countries', 'CountryController@index')->name('countries');

// Lazy Eager Load
#Route::get('countries/{condition}', 'CountryController@lazyCountries');

Route::get('cities', 'CountryController@getCities');
Route::get('cities/{keyword}', 'CountryController@getCitiesByStreets');

/*Полиморфные связи*/
Route::get('media-comments', 'MediaCommentController@index');
Route::get('video/{videoId}/comments', 'MediaCommentController@videoComments');
Route::get('photo/{photoId}/comments', 'MediaCommentController@photoComments');

Route::get('show-tags/{id}/{type}', 'TagController@showTags');
Route::get('tags', 'TagController@index');

/*
 * 1 вариант подключения посредника
 * */
Route::get('middle', function (){
    return view('welcome');
})->middleware('add_hash');

// Ресурс контроллер
Route::resource('countries', 'Resource\CountryController', ['expect' => ['edit', 'update']]);

Route::get('locale/{locale}', 'SystemController@changeLocale')->name('change_locale');