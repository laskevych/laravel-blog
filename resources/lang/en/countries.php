<?php

 return [
     'list' => [
         'users_title' => 'All Users',
         'description' => 'This page have :users list',
         'add_user' => 'Add user',
     ],
     'other' => [
         'change_lang' => 'Change Lang',
         'locale' => 'ru',
     ]
 ];