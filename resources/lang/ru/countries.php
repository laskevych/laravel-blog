<?php

 return [
     'list' => [
         'users_title' => 'Все пользователи',
         'description' => 'Это старница со списком :users',
         'add_user' => 'Добавить пользователя',
     ],
     'other' => [
         'change_lang' => 'Сменить язык',
         'locale' => 'en',
     ]
     
 ];