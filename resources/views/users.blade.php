@extends('index', ['meta_title' => 'Пользователи'])

{{--@section('meta_title', 'Имена')--}}

@section('content')

    @if($users)
        <h1>@lang('countries.list.users_title')</h1>
        <a href="users/add" class="btn btn-primary btn-sm">@lang('countries.list.add_user')</a>
        <p>@lang('countries.list.description', ['users'=>'пользователи'])</p>
        <ul class="list-group">
            @foreach($users as $user)
            <li class="list-group-item">
                <a href="{{ route('user', $user->id) }}">{{ $user->name }}</a> {{ $user->email }}
            </li>
            @endforeach

        </ul>
    @else
        <div class="title m-b-md">
            No users
        </div>
    @endif

    <a href="{{ route('change_locale', __('countries.other.locale')) }}">@lang('countries.other.change_lang')</a>
@endsection