@extends('index', ['meta_title' => 'Пользователи'])

{{--@section('meta_title', 'Имена')--}}

@section('content')

    @if($users)
        <h1>Пользователи</h1>
        <a href="users/add" class="btn btn-primary btn-sm">Добавить пользователя</a>
        <ul class="list-group">
            @foreach($users as $user)
            <li class="list-group-item">
                {{ $user->name }} {{ $user->email }}
            </li>
            @endforeach

        </ul>
    @else
        <div class="title m-b-md">
            No users
        </div>
    @endif
@endsection