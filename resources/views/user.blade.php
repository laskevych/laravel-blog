@extends('index', ['meta_title' => 'Пользователь'])


@section('content')

    <a href="{{ route('users') }}">All Users</a>
    @if($user)
        <div class="title m-b-md">
            {{ $user->name }} {{ $user->email }}
        </div>
        @if($user->comments)
            <ul class="list-group">
                @foreach($user->comments as $comment)
                    <li class="list-group-item">
                        {{ $comment->text }}
                    </li>
                @endforeach

            </ul>
        @endif
    @else
        <div class="title m-b-md">
            No user
        </div>
    @endif
@endsection