@extends('index', ['meta_title' => 'Пользователь'])


@section('content')

    @if($user)
        <div class="title m-b-md">
            {{ $user->name }} {{ $user->phone }}
        </div>
    @else
        <div class="title m-b-md">
            No user
        </div>
    @endif
@endsection