@extends('index', ['meta_title' => 'Имена'])

{{--@section('meta_title', 'Имена')--}}

@section('content')

    @if($names)
        <div class="title m-b-md">
            Имена
        </div>
        <div class="links">
            @foreach($names as $name)
                <a href="/names/{{ $name->id }}">{{ $name->name }}</a>
            @endforeach
        </div>
    @else
        <div class="title m-b-md">
            No users
        </div>
    @endif
@endsection