<?php

use Illuminate\Database\Seeder;

class StreetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('streets')->insert([
            'street' => 'ул. Шевченка',
            'city_id' => 1
        ]);

        DB::table('streets')->insert([
            'street' => 'ул. Леси Украинки',
            'city_id' => 1
        ]);

        DB::table('streets')->insert([
            'street' => 'ул. Сковороды',
            'city_id' => 1
        ]);

        DB::table('streets')->insert([
            'street' => 'ул. Мазепы',
            'city_id' => 1
        ]);

        DB::table('streets')->insert([
            'street' => 'ул. Пушкина',
            'city_id' => 2
        ]);

        DB::table('streets')->insert([
            'street' => 'ул. Толстого',
            'city_id' => 2
        ]);
    }
}
