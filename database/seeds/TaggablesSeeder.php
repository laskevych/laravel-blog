<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TaggablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taggables')->insert([
            'tag_id' => 1,
            'taggable_id' => 1,
            'taggable_type' => 'App\Video',
        ]);

        DB::table('taggables')->insert([
            'tag_id' => 3,
            'taggable_id' => 1,
            'taggable_type' => 'App\Video',
        ]);

        DB::table('taggables')->insert([
            'tag_id' => 3,
            'taggable_id' => 2,
            'taggable_type' => 'App\Photo',
        ]);

        DB::table('taggables')->insert([
            'tag_id' => 2,
            'taggable_id' => 3,
            'taggable_type' => 'App\Photo',
        ]);

        DB::table('taggables')->insert([
            'tag_id' => 1,
            'taggable_id' => 1,
            'taggable_type' => 'App\Photo',
        ]);

        DB::table('taggables')->insert([
            'tag_id' => 2,
            'taggable_id' => 1,
            'taggable_type' => 'App\Photo',
        ]);
    }
}
