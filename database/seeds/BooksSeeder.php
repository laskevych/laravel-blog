<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'title' => 'PHP Books',
            'description' => str_random('500'),
            'price' => 10.99
        ]);

        DB::table('books')->insert([
            'title' => 'Java Books',
            'description' => str_random('250'),
            'price' => 15.99
        ]);

        DB::table('books')->insert([
            'title' => 'JS Books',
            'description' => str_random('800'),
            'price' => 5.99
        ]);
    }
}
