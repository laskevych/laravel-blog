<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MediaCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_comments')->insert([
            'comment' => 'Классное фото',
            'commentable_id' => 1,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => 'Классное фото!!!',
            'commentable_id' => 1,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => 'Ну такое себе фото',
            'commentable_id' => 2,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => str_random('10'),
            'commentable_id' => 3,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => str_random('10'),
            'commentable_id' => 4,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => str_random('10'),
            'commentable_id' => 3,
            'commentable_type' => 'App\Photo',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => 'Видео просто супер',
            'commentable_id' => 1,
            'commentable_type' => 'App\Video',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => 'Да оно супер',
            'commentable_id' => 2,
            'commentable_type' => 'App\Video',
            'user_id' => 0,
        ]);

        DB::table('media_comments')->insert([
            'comment' => 'Я не понял.',
            'commentable_id' => 2,
            'commentable_type' => 'App\Video',
            'user_id' => 0,
        ]);
    }
}
