<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'tag' => 'Отпуск'
        ]);

        DB::table('tags')->insert([
            'tag' => 'Работа'
        ]);

        DB::table('tags')->insert([
            'tag' => 'Другое'
        ]);
    }
}
