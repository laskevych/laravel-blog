<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaComment extends Model
{
    /**
     * Получить все модели, обладающие commentable.
    */
    public function commentable()
    {
        return $this->morphTo();
    }
}
