<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * Получить все комментарии для Видео
     */
    public function comments()
    {
        return $this->morphMany(MediaComment::class, 'commentable');
    }

    /**
     * Получить все теги для фото
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
