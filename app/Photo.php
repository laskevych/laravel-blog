<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * Получить все комментарии для фото
     */
    public function comments()
    {
        return $this->morphMany(MediaComment::class, 'commentable');
    }

    /**
     * Получить все теги для фото
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
