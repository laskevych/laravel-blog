<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Books extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title', 'description', 'price', 'images'
    ];
    
    protected  $dates = ['delete_at'];
    
    public $timestamps = false;
}
