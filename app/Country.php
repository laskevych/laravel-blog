<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Country extends Model
{
    /*Отношение ко многим через*/
    public function streets()
    {
        return $this->hasManyThrough(Street::class, City::class);
    }
    
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
