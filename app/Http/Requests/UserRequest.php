<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
    }
    
    public function messages()
    {
        /*
         * Можно указывать вот так.
         * */
        return [
            'name.required' => 'Не забывайте вводить имя',
            'name.min' => 'Имя должно быть от :min символов',
            'email.required' => 'Введи email!',
            'password.required' => 'А где пароль?',
            'password.min' => 'Пароль от :min символов дубина :D'
        ];

        /*
         * Можно указывать общие сообщения для всех required.
         * */
        /*return [
            'required' => 'Пустое поле :attribute'
        ];*/
    }
}
