<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BooksController extends Controller
{
    public function index(Request $request)
    {
        //return view('form');
//        dump($request->path());
//        dump($request->fullUrl());
//        dump($request->is('books'));
//        dump($request->is('books/1'));
//        dump($request->query);
//        dump($request->method());
//        dump($request->isMethod('POST'));

        // Все три способа работают одинаково
        /*dump($request->get('name'));
        dump($request->input('name'));
        dump($request->name);*/
        
        /*Курсоры*/
        /*foreach (Books::where('price', '>', 2)->cursor() as $item){
            dump($item);
        }*/

        /*Исключения «не найдено»*/
//        $book = Books::findOrFail(1);
//        $book = Books::where('price', '>', 20)->firstOrFail();
//        dump($book);
        
        /*Агрегатные функции*/
//        $count = Books::count();
//        dump($count);
//        $max = Books::max('price');
//        dump($max);
//        $sum = Books::sum('price');
//        dump($sum);
        
        /*Можно вручную писать запрос where*/
//        $books = Books::whereRaw("price > ? AND images='no_image.png'", [5])->get();
//        dump($books);
        
        /*Изменения*/
        /*$book = Books::find(1);
        $book->price = 24.99;
        $book->save();

        $book = Books::find(1);
        dump($book->price);*/

        /*Массовые изменения*/
        /*Books::where('price', '<', 15)
            ->where('images', 'no_image.png')
            ->update(['price'=>14.99]);*/
        
        /*Создание записи*/
        /*Books::create([
            'title' => 'Ruby Good Practice',
            'price' => 3.15,
            'images' => 'ruby_logo.png'
        ]);*/
        
        /*firstOrCreate / firstOrNew*/
        /**
         * firstOrCreate() - создаст запись если такой записи нет 
         * firstOrNew() - создаст экземпляр класса, но ничего в бд не запишет 
         * updateOrCreate() - обновить, если запсь есть. Создать если ее нет
         */
        
        /*Мягкое удаление*/
        // Нужно создать поле в базе и в модели прописать использование трейта SoftDeletes
        //Books::where('title', 'Ruby Good Practice')->delete();

        // Удалена ли запись мягко
        /*if ($book->trashed()) {
            //
        }*/
        
        /**
         * Вытянуть все записи. Даже удаленные
         * Еще есть метод onlyTrashed()
         * 
         * Восстановить запись после мягкого удаления
         * restore();
         * 
         * Полсное удаление записи
         * forceDelete():
         * 
         */
        /*$books = Books::withTrashed()
            ->where('price', '>', 2)
            ->get();
        dump($books);*/
        
        
        
    }

    // Работа с файлами
    public function sendForm(Request $request)
    {
        Storage::get('test.jpeg');
//        dump($request->input('user_name'));
        //dump($request->all());
//        dump($request->file('image'));
//        dump($request->file('image')->path());
//        dump($request->file('image')->extension());
//        dump($request->file('image')->getClientSize());
//        dump($request->file('image')->getClientOriginalName());
        $file = $request->file('image')->store('images', 'public');
        //return view('form');
    }
}
