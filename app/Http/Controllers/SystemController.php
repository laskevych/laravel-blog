<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SystemController extends Controller
{
    public function changeLocale(string $locale)
    {
        /*$loc = App::getLocale();
        dd($loc);*/
        if (in_array($locale, ['ru', 'en'])) {
            Session::put('locale', $locale);
            //App::setLocale('ru');
        }
        
        // Вернем назад пользователя
        return redirect()->back();
    }
}
