<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        /*$country = Country::find(2);
        dump($country);
        dump($country->streets->all());*/
        
        $countries = Country::with('streets', 'cities')->get();
        foreach ($countries as $country) {
            dump($country->cities);
            dd($country->streets);
        }
    }
    
    // Нетерпеливая загрузка
    public function getCities()
    {
        //$cities = City::all();
        $cities = City::with('streets')->get();
        foreach ($cities as $city) {
            dump($city->streets);
        }
    }

    // Нетерпеливая загрузка
    public function getCitiesByStreets(string $keyword)
    {
        $cities = City::with(['streets' => function($query) use ($keyword) {
            $query->where('street', 'like', "%{$keyword}%");
        }])->get();
        foreach ($cities as $city) {
            dump($city->streets);
        }
    }
    
    // Ленивая нетерпеливая загрузка
    public function lazyCountries(string $condition)
    {
        $countries = Country::get();
        if ($condition == 'true') {
            $countries->load('cities');
        }
        
        dump($countries);
    }
}
