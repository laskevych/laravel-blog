<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Name;
use phpDocumentor\Reflection\DocBlock;

class NamesController extends Controller
{
    public function index()
    {
        //$name = Name::find([1, 2]);
        //$name2 = Name::whereIn('id', [3, 4])->get();
        $names = Name::all();
        //$names = Name::where('name', 'like', '%a%')->get();
        //$name = Name::visible(0)->get()->first();
        //$name->c_date = Carbon::create(2018, 02, 05);
        return view('names', compact('names'));
    }
    
    public function show($id)
    {
        $user = Name::select('id', 'name', 'phone', 'visible')->where('id', $id)/*->where('visible', 1)*/->get()->first();
        //$user = Name::findOrFail($id); // отдать 404 если нет такого юзера можно передавать массивы
        //$user = Name::firstOrFail($id); // отдать 404 если нет такого юзера. Вернет только 1. Если хотя бы одна такая запись есть.

        return view('name', compact('user'));
    }
    
    public function createName()
    {
        
    }
}
