<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();
        return view('users', compact('users'));
    }


    /*
     * Так делать не правильно. Мы нарушаем принцип единой отвественности. У нас Контроллер - валидирует, добавляет, возвращает. Это плохо.
     * Нужно создать класс Request для User как сделано ниже
     * */
    /*public function addUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        User::create($request->all());
        return redirect('users');
        dd($request->all());
    }*/
    
    public function addUser(UserRequest $request)
    {
        User::create($request->all());
        return redirect('users');
        //dd($request->all());
    }
    
    public function getUser($userId)
    {
        $user = User::find($userId);
        return view('user', compact('user'));
    }
    
    public function getUserComments($userId)
    {
        $user = User::find($userId);
        //dd($user->comments);
        return view('user', compact('user'));
    }
    
    public function getRolesByUser($userId)
    {
        $user = User::find($userId);
        dump($user->roles);
    }
    
    public function getUsersHasComments()
    {
        $users = User::has('comments')->get();
        dump($users);
    }
}
