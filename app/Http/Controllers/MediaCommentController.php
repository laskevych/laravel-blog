<?php

namespace App\Http\Controllers;

use App\MediaComment;
use App\Photo;
use App\Video;
use Illuminate\Http\Request;

class MediaCommentController extends Controller
{
    public function index()
    {
        $comment = MediaComment::find(1);
        dump($comment);
        dump($comment->commentable);
    }
    
    public function photoComments($photoId)
    {
        $photo = Photo::find($photoId);
        dump($photo->comments);
    }

    public function videoComments($videoId)
    {
        $video = Video::find($videoId);
        dump($video->comments);
    }
}
