<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::all();
        dump($comments);
    }
    
    public function getUser($commentId)
    {
        $comment = Comment::find($commentId);
        dump($comment->user);
    }
    
    public function getBook($commentId)
    {
        $comment = Comment::find($commentId);
        dump($comment->book);
    }
}
