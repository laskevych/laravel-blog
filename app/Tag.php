<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function photos()
    {
        return $this->morphedByMany(Photo::class, 'taggable', 'taggables', 'tag_id');
    }

    public function videos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
    }
}
