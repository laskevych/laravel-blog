<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Name extends Model
{
    protected $fillable = ['name', 'phone', 'visible']; // что можно записывать в базу данных
    protected $guarded = ['created_at']; // какие колонки защищены от записи
    
    public function scopeVisible($query, $visible)
    {
        return $query->where('visible', $visible);
    }
    
    public function toLowerCaseAttribute()
    {
        return strtolower($this->name);
    }
    
    /*public function nameAndPhoneAttribute()
    {
        return "$this->name $this->phone";
    }*/
}
